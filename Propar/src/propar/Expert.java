package propar;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mesExceptions.CaracteresInvalidesException;
import mesExceptions.SaisiVideException;

public class Expert extends Employe {

	/**
	 * Contructor Expert 
	 * @param nom
	 * @param prenom
	 * @param grade
	 * @param login
	 * @param mdp
	 */
	
	public Expert(String nom, String prenom, String grade, String login, String mdp) {
		super(nom, prenom, grade, login, mdp);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Ajoute une personne dans la liste des employ�s 
	 * @param employes
	 * @throws SaisiVideException
	 * @throws CaracteresInvalidesException
	 */

	public static void ajouterPersonnel(List<Employe> employes) throws SaisiVideException, CaracteresInvalidesException {
		Scanner lire = new Scanner(System.in);
		
			System.out.println("Nom de l'employ� : ");
			String nom = lire.next();
			lire.nextLine();
			
			// char tabCarac[] = {'&','~','#','{' ,'[','|','@',']','}','�','�', ')', '(', '_', '+', '"', '=', '*', '!', '�', '/', ':', ';', '.', '?', ',', '<', '>', '%' };	
			String tabCarac[] = {"&","~","#","{" ,"[","|","@","]","}","�","�", ")", "(", "_", "+", "\"", "=", "*", "!", "�", "/", ":", ";", ".", "?", ",", "<", ">", "%" };

			if (nom.length()==0) {
				throw new SaisiVideException();
			}
			for(int i = 0; i < tabCarac.length; i++) {
				 if (nom.contains(tabCarac[i])) {
						throw new CaracteresInvalidesException();
				}	
			}
			

			
			System.out.println("Pr�nom de l'employ� : ");
			String prenom = lire.next();
			lire.nextLine();
			if (prenom.length()==0) {
				throw new SaisiVideException();
			}
			for(int i = 0; i < tabCarac.length; i++) {
				 if (nom.contains(tabCarac[i])) {
						throw new CaracteresInvalidesException();
				}	
			}

			System.out.println("Grade de l'employ� (Expert/Senior/Apprenti) : ");
			String grade = lire.next();
			lire.nextLine();

			while (grade != "Expert" || grade != "Senior" || grade != "Apprenti") {
				if (grade.equals("Expert") || grade.equals("Senior") || grade.equals("Apprenti")) {
					break;
				} else {
					System.out.println("Grade incorrect, veuillez choisir entre : Expert, Senior, Apprenti");
					grade = lire.next();
					lire.nextLine();
				}
			}

			if (grade.equals("Expert")) {
				operationMax = Constante.getOperationMaxExpert();
			} else if (grade.equals("Senior")) {
				operationMax = Constante.getOperationMaxSenior();
			} else if (grade.equals("Apprenti")) {
				operationMax = Constante.getOperationMaxApprentit();
			}

			System.out.println("Login de l'employ� : ");
			String loginStr = lire.next();
			lire.nextLine();
			if (loginStr.length()==0) {
				throw new SaisiVideException();
			}
			for(int i = 0; i < tabCarac.length; i++) {
				 if (nom.contains(tabCarac[i])) {
						throw new CaracteresInvalidesException();
				}	
			}

				for (Employe element : employes) {
					if (element.getLogin().equals(loginStr)) {
						System.out.println("Ce login existe d�j�, veuillez en saisir un autre :");
						loginStr = lire.next();
						lire.nextLine();
					}
				}
			

			System.out.println("Mot de passe de l'employ� : ");
			String mdp = lire.next();
			lire.nextLine();
			if (mdp.length()==0) {
				throw new SaisiVideException();
			}
			for(int i = 0; i < tabCarac.length; i++) {
				 if (nom.contains(tabCarac[i])) {
						throw new CaracteresInvalidesException();
				}	
			}
			

			Employe employe = new Employe(nom, prenom, grade, loginStr, mdp);
			employes.add(employe);

			System.out.println("Vous avez bien ajout� l'employ� " + nom + " " + prenom + ", " + grade + ".");


	}
}
