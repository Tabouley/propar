package propar;

public class Constante {
	private static final int OPERATION_MAX_EXPERT = 5;
	private static final int OPERATION_MAX_SENIOR = 3;
	private static final int OPERATION_MAX_APPRENTIT = 1;

	public static int getOperationMaxExpert() {
		return OPERATION_MAX_EXPERT;
	}

	public static int getOperationMaxSenior() {
		return OPERATION_MAX_SENIOR;
	}

	public static int getOperationMaxApprentit() {
		return OPERATION_MAX_APPRENTIT;
	}
}