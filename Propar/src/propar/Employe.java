package propar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import mesExceptions.CaracteresInvalidesException;
import mesExceptions.SaisiVideException;

public class Employe {
	private String nom;
	private String prenom;
	protected String grade;
	private String login;
	private String mdp;
	static List<Operation> operations = new ArrayList<Operation>();
	static List<Operation> operationPerso = new ArrayList<Operation>();
	protected static int operationMax;
	int cptOperation;
	String tabCarac[];

	
	
	/**
	 * Constructor Employe
	 * 
	 * @param nom
	 * @param prenom
	 * @param grade
	 * @param login
	 * @param mdp
	 */
	public Employe(String nom, String prenom, String grade, String login, String mdp) {
		Scanner lire = new Scanner(System.in);
		this.nom = nom;
		this.prenom = prenom;
		this.grade = grade;
		this.login = login;
		this.mdp = mdp;
		boolean etatGrade = false;
		while (etatGrade = false) {
			if (this.grade.equals("Expert")) {
				operationMax = Constante.getOperationMaxExpert();
				etatGrade = true;
			} else if (this.grade.equals("Senior")) {
				operationMax = Constante.getOperationMaxSenior();
				etatGrade = true;
			} else if (this.grade.equals("Apprenti")) {
				operationMax = Constante.getOperationMaxApprentit();
				etatGrade = true;
			} else {
				System.out.println("Grade incorrect, veuillez choisir entre : Expert, Senior, Apprenti");
				etatGrade = false;
				this.grade = lire.nextLine();
			}
		}
	}
	
	

	/**
	 *Liste les op�rations termin�es
	 */
	public static void listerOperationTermine() {
		for (Operation operation : operations) {
			if (operation.isEtat() == true) {
				System.out.println(operation.toString());
			}
		}
	}

	
	
	
	/**
	 * Liste et trie toutes les op�rations
	 */
	public static void listerOperation() {

		Collections.sort(operations, new OperationComparator());

		for (Operation operation : operations) {
			if (operation.isEtat() == false) {
				System.out.println(operation.toString());
			}
		}

	}
	
	
	
	
/**
 * Liste les op�rations personnelles 
 * @param login
 */
	public static void listerOperationPerso(String login) {

		for (Operation operation : operationPerso) {
			if ((operation.getLoginEmploye().equals(login))) {
				System.out.println(operation.toString());
			}

		}
	}
/**
 *  Ajoute une op�ration dans la liste .
 * @param employes
 * @param login
 * @throws SaisiVideException
 * @throws CaracteresInvalidesException
 */
	public static void ajouterOperation(List<Employe> employes, String login)
			throws SaisiVideException, CaracteresInvalidesException {

		// char tabCarac[] = {'&','~','#','{' ,'[','|','@',']','}','�','�', ')', '(',
		// '_', '+', '"', '=', '*', '!', '�', '/', ':', ';', '.', '?', ',', '<', '>',
		// '%' };
		String tabCarac[] = { "&", "~", "#", "{", "[", "|", "@", "]", "}", "�", "�", ")", "(", "_", "+", "\"", "=", "*",
				"!", "�", "/", ":", ";", ".", "?", ",", "<", ">", "%" };

		for (Employe employe : employes) {
			if ((employe.getLogin().equals(login))) {
				if (employe.getGrade().equals("Expert")) {
					if (employe.getCptOperation() < 5) {
						Scanner sc = new Scanner(System.in);
						System.out.println("Grosse, Moyenne ou Petite Op�ration ?");
						String nomOperation = sc.nextLine();
						System.out.println("Nom du client ?");
						String nomClient = sc.nextLine();
						if (nomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (nomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						System.out.println("Pr�nom du client ?");
						String prenomClient = sc.nextLine();
						if (prenomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (prenomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						Operation operation = new Operation(nomOperation, nomClient, prenomClient);
						operations.add(operation);
						operationPerso.add(operation);
						employe.setUpCptOperation();
						System.out.println("Vous avez bien ajout� l'op�ration.");
					} else {
						System.out.println("Vous avez trop de t�ches en cours.");
					}
				}

				if (employe.getGrade().equals("Senior")) {
					if (employe.getCptOperation() < 3) {
						Scanner sc = new Scanner(System.in);
						System.out.println("Grosse, Moyenne ou Petite Op�ration ?");
						String nomOperation = sc.nextLine();
						System.out.println("Nom du client ?");
						String nomClient = sc.nextLine();
						if (nomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (nomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						System.out.println("Pr�nom du client ?");
						String prenomClient = sc.nextLine();
						if (prenomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (prenomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						Operation operation = new Operation(nomOperation, nomClient, prenomClient);
						operations.add(operation);
						operationPerso.add(operation);
						employe.setUpCptOperation();
						System.out.println("Vous avez bien ajout� l'op�ration.");
					} else {
						System.out.println("Vous avez trop de t�ches en cours.");
					}
				}

				if (employe.getGrade().equals("Apprenti")) {
					if (employe.getCptOperation() < 1) {
						Scanner sc = new Scanner(System.in);
						System.out.println("Grosse, Moyenne ou Petite Op�ration ?");
						String nomOperation = sc.nextLine();
						System.out.println("Nom du client ?");
						String nomClient = sc.nextLine();
						if (nomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (nomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						System.out.println("Pr�nom du client ?");
						String prenomClient = sc.nextLine();
						if (prenomClient.length() == 0) {
							throw new SaisiVideException();
						}
						for (int i = 0; i < tabCarac.length; i++) {
							if (prenomClient.contains(tabCarac[i])) {
								throw new CaracteresInvalidesException();
							}
						}
						Operation operation = new Operation(nomOperation, nomClient, prenomClient);
						operations.add(operation);
						operationPerso.add(operation);
						employe.setUpCptOperation();
						System.out.println("Vous avez bien ajout� l'op�ration.");
					} else {
						System.out.println("Vous avez trop de t�ches en cours.");
					}
				}
			}
		}
	}
	
	/**
	 * Termine une operation
	 * @param employes
	 * @param login
	 */

	public static void terminerOperation(List<Employe> employes, String login) {

		for (Employe employe : employes) {
			if (employe.getLogin().equals(login)) {
				for (Operation op : operationPerso) {
					if (op.getLoginEmploye().equals(login)) {
						if (!op.isEtat()) {
							op.setEtat(true);
							System.out.println("Op�ration termin�e :");
							System.out.println(op.toString());
							employe.setDownCptOperation();
							return;
						}
					}
				}
			}
		}

	}

	public int getCptOperation() {
		return cptOperation;
	}

	public void setUpCptOperation() {
		this.cptOperation = this.cptOperation + 1;
	}

	public void setDownCptOperation() {
		this.cptOperation = this.cptOperation - 1;
	}

	public int getOperationMax() {
		return this.operationMax;
	}

	public void setOperationMax(int operationMax) {
		this.operationMax = operationMax;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom1) {
		this.nom = nom1;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom1) {
		this.prenom = prenom1;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

}