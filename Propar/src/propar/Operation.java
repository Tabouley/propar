package propar;

import java.util.List;
import java.util.Scanner;

public class Operation {
	private String nom;
	private double prix;
	private boolean etat;
	private String nomClient;
	private String prenomClient;
	private String nomEmploye;
	private String prenomEmploye;
	private String loginEmploye;
	
	
	/**
	 * Constructor Operation
	 * @param nom
	 * @param nomClient
	 * @param prenomClient
	 */

	public Operation(String nom, String nomClient, String prenomClient) {
		Scanner lire = new Scanner(System.in);
		this.nom = nom;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.nomEmploye = Menu.utilisateurNom;
		this.prenomEmploye = Menu.utilisateurPrenom;
		this.loginEmploye = Menu.utilisateurLogin; 
		this.etat = false;
		boolean typeOperation = false;
		while (typeOperation == false) {
			if (this.nom.equals("Grosse")) {
				this.prix = 10000;
				typeOperation = true;
			} else if (this.nom.equals("Moyenne")) {
				this.prix = 2500;
				typeOperation = true;
			} else if (this.nom.equals("Petite")) {

				this.prix = 1000;
				typeOperation = true;
			} else {
				System.out.println(
						"Le type de l'op�ration est incorrect, veuillez choisir entre : Grosse, Moyenne, Petite");

				this.nom = lire.nextLine();
			}
		}
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	/**
	 * Calcule la recette des op�rations termin�es .
	 * @param operations
	 */
	public static void calculRecette(List<Operation> operations) {
		double recette = 0;
		if (Employe.operations.size()==0) {
			System.out.println("La recette des op�rations termin�es a une valeur de " + recette + " euros.");
		}
		else {
		for (Operation operation : Employe.operations) {
			if(operation.isEtat() == true) {
				recette=recette+operation.getPrix();
			}
		
		}
		System.out.println("La recette des op�rations termin�es a une valeur de " + recette + " euros.");
	}
	}


	@Override
	public String toString() {
		if(etat == false) {
			return nomEmploye + " " + prenomEmploye + " : Op�ration de type " + nom + ", " + prix + " pour le client : " + nomClient + " "
					+ prenomClient + " en cours.";
		}
		else {
			return nomEmploye + " " + prenomEmploye + " : Op�ration de type " + nom + ", " + prix + " pour le client : " + nomClient + " "
					+ prenomClient + " termin�e.";
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public String getNomEmploye() {
		return nomEmploye;
	}

	public void setNomEmploye(String nomEmploye) {
		this.nomEmploye = nomEmploye;
	}

	public String getPrenomEmploye() {
		return prenomEmploye;
	}

	public void setPrenomEmploye(String prenomEmploye) {
		this.prenomEmploye = prenomEmploye;
	}
	
	public String getLoginEmploye() {
		return loginEmploye;
	}
	
	public void setLoginEmploye(String loginEmploye) {
		this.loginEmploye = loginEmploye;
	}

	


}
