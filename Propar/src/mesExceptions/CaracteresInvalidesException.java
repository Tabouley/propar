package mesExceptions;

public class CaracteresInvalidesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Message qui s'affiche quand l'exception est catch
	 */
	public CaracteresInvalidesException() {
		super("Saisie incorrect.");
	}

}
