package mesExceptions;

public class SaisiVideException extends Exception {

	
	private static final long serialVersionUID = 1L;

	/**
	 * Message qui s'affiche quand l'exception est catch
	 */
	public SaisiVideException() {
		super("Saisie vide, retour au Menu.");
	}
}
